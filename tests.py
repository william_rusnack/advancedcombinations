import unittest

from AdvancedCombinations import AdvancedCombinations as AC

from pprint import pprint



class AdvancedCombinationsMethodsTests(unittest.TestCase):

  def test___iter__(self):
    test_combos = AC(
        (1,2),
        (3,4,5),
        (6,7)
      )
    for i in range(2):
      self.assertEqual(
        tuple(test_combos),
        (
          (1,3,6),
          (1,3,7),
          (1,4,6),
          (1,4,7),
          (1,5,6),
          (1,5,7),
          (2,3,6),
          (2,3,7),
          (2,4,6),
          (2,4,7),
          (2,5,6),
          (2,5,7),
        )
      )

    self.assertEqual(
      set((('hi', combo['hi']), ('bye', combo['bye']), ('here', combo['here']))
        for combo in AC(hi=(1,2), bye=(3,4,5), here=(6,7))),
      set(
        (('hi', hi), ('bye', bye), ('here', here)) for hi, bye, here in (
          (1, 3, 6),
          (1, 3, 7),
          (1, 4, 6),
          (1, 4, 7),
          (1, 5, 6),
          (1, 5, 7),
          (2, 3, 6),
          (2, 3, 7),
          (2, 4, 6),
          (2, 4, 7),
          (2, 5, 6),
          (2, 5, 7),
        )
      )
    )

  def test_disable(self):
    test_combos = AC(
        (1,2),
        (3,4,5),
        (6,7)
      )

    test_combos.disable(lambda combo: 1 in combo)
    self.assertEqual(
      tuple(test_combos),
      (
        (2,3,6),
        (2,3,7),
        (2,4,6),
        (2,4,7),
        (2,5,6),
        (2,5,7),
      )
    )

    test_combos.disable(lambda combo: True)
    self.assertEqual(
      tuple(test_combos),
      ()
    )
    test_combos.enable_all()

    self.assertEqual(
      tuple(test_combos),
      (
        (1,3,6),
        (1,3,7),
        (1,4,6),
        (1,4,7),
        (1,5,6),
        (1,5,7),
        (2,3,6),
        (2,3,7),
        (2,4,6),
        (2,4,7),
        (2,5,6),
        (2,5,7),
      )
    )

  def test_filter_out_all_greater_or_equal(self):
    test_combos = AC(
      (1,2),
      (3,4,5),
      (6,7)
    )

    test_combos.disable(AC.filter_out_all_greater_or_equal((1, 3, 6)))
    self.assertEqual(
      tuple(test_combos),
      ()
    )
    test_combos.enable_all()

    test_combos.disable(AC.filter_out_all_greater_or_equal((1, 3, 7)))
    self.assertEqual(
      tuple(test_combos),
      (
        (1, 3, 6),
        (1, 4, 6),
        (1, 5, 6),
        (2, 3, 6),
        (2, 4, 6),
        (2, 5, 6)
      )
    )


if __name__ == '__main__': unittest.main()
