from itertools import product


class AdvancedCombinations:
  def __init__(self, *args, **kargs):
    if args and not kargs:
      self.to_combine = args
      self.add_labels = False

    elif kargs and not args:
      self.to_combine = tuple((key, val) for key, val in kargs.items())
      self.add_labels = True

    else:
      raise ValueError('Both args and kargs cannot be defined.')

    self.disable_functions = []

  def __iter__(self):
    if self.add_labels:
      for combo in product(*(a[1] for a in self.to_combine)):
        if not any(df(combo) for df in self.disable_functions):
          yield {key: val for key, val in zip(
            (kv[0] for kv in self.to_combine),
            combo
            )
          }
    else:
      for combo in product(*self.to_combine):
        if not any(df(combo) for df in self.disable_functions):
          yield combo

  def disable(self, filter_out_function):
    self.disable_functions.append(filter_out_function)

  def enable_all(self):
    self.disable_functions = []

  @staticmethod
  def filter_out_all_greater_or_equal(min_combo):
    return lambda combo: all(min_val <= val for min_val, val in zip(min_combo, combo))
