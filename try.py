from AdvancedCombinations import AdvancedCombinations as AC

from pprint import pprint

test_combos = AC(
        (1,2),
        (3,4,5),
        (6,7)
      )

filter_func = AC.filter_out_all_greater_or_equal((2,3,6))

# pprint(tuple(test_combos))
# print()
for c in test_combos:
  print(c, filter_func(c.combo))

