from setuptools import setup

setup(name='AdvancedCombinations',
      version='0.0.0',
      license='MIT',
      description='Creates all combinations without repeats of from multiple iterators.',
      author='William Rusnack',
      author_email='williamrusnack@gmail.com',
      url='https://github.com/BebeSparkelSparkel/jackedCodeTimerPY',
      classifiers=['Development Status :: 2 - Pre-Alpha', 'Programming Language :: Python :: 3'],
     )
